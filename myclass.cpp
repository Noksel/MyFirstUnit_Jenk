#include "myclass.h"

MyClass::MyClass()
{

}
/*!
\brief Так оформляются методы
Сравнивает два числа и возвращает наибольшее

\param[in] a Первое число
\param[in] b Второе число
\return Наибольшее из двух чисел

Код функции выглядит следующим образом:

\code
int MyClass::max(int a, int b)
{
    int m;
    if (a>b)
        m=a;
    else
        m=b;
    return m;
}
\endcode
*/
int MyClass::max(int a, int b)
{
    int m;
    if (a>b)
        m=a;
    else
        m=b;
    return m;
}
