#-------------------------------------------------
#
# Project created by QtCreator 2016-08-31T14:58:19
#
#-------------------------------------------------

QT       += testlib

#QT       -= gui

TARGET = tst_myfirstunittest
CONFIG   += console
CONFIG   -= app_bundle

TEMPLATE = app


SOURCES += tst_myfirstunittest.cpp \
    myclass.cpp
DEFINES += SRCDIR=\\\"$$PWD/\\\"

HEADERS += \
    myclass.h
