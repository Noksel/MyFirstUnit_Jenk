#include <QString>
#include <QtTest>
#include "myclass.h"

/*!
    \mainpage
    \brief Демонстрация генерации документации на основе аннотированного исходного кода
    \author Иванов Иван Иванович
    \version 1.0.1
    \date Сентябрь 2016 года
   Полное описание проекта
   В данном проекте реализован класс с методом для определения наибольшего из двух чисел.
   Реализован класс для тестирования метода определения наибольшего из двух чисел двумя смособами.
    \note Так оформляются заметки.
    \note Необходим рефакторинг кода
    \warning Так оформляются предупреждения
    \warning При переносе на другую платформу необходима переконфигурация настроек проекта
*/


/*!
    \class MyFirstUnitTest
    \brief Класс для тестирования работы класса #MyClass
    \author Иванов Иван Иванович
    \version 0.4
    \date август 2016 года

    Тестирование производится несколькими методами. В том числе табличным
*/
class MyFirstUnitTest : public QObject
{
    Q_OBJECT

public:
    MyFirstUnitTest();

private Q_SLOTS:
    void initTestCase();
    void cleanupTestCase();
    void TestMax();
    void testCase1();
    void TablTestMax_data();
    void TablTestMax();
};

/*!
 * \brief Конструктор класса #MyFirstUnitTest
 */
MyFirstUnitTest::MyFirstUnitTest()
{
}

/*!
 * Слот выполняется до запуска тестов.
 * Предназначен для проверки корректности создания объектов,
 * инициализации ресурсов и т.д.
 */
void MyFirstUnitTest::initTestCase()
{
}

/*!
 * Слот выполняется после запуска тестов.
 * Предназначен для проверки корректности уничтожения объектов,
 * освобождения ресурсов и т.д.
 */
void MyFirstUnitTest::cleanupTestCase()
{
}

/*!
 * Простой тест по сравнению результата работы метода myClass.max и ожидаемого значения.
 * \warning при наличии ошибки FAIL выбрасывает только первый проваленный тест.
 */
void MyFirstUnitTest::TestMax()
{
    MyClass myClass;      // Создаём экземпляр нашего класса
    QCOMPARE(myClass.max(25, 0), 25);
    QCOMPARE(myClass.max(-12, -5), -5);
    QCOMPARE(myClass.max(2007, 2007), 2007);
    QCOMPARE(myClass.max(-12, 5), 5);
}

/*!
 * Тест на истинность выражения
 */
void MyFirstUnitTest::testCase1()
{
    QVERIFY2(true, "Failure");
}

/*!
 * Инициализация данных для табличного теста.
 * \note при наличии ошибки FAIL выбрасывают все пероваленные тесты.
 */
void MyFirstUnitTest::TablTestMax_data()
{

    QTest::addColumn<int>("first");
    QTest::addColumn<int>("second");
    QTest::addColumn<int>("expected");

    QTest::newRow("Test data 1") << 0 << 0 << 0;
    QTest::newRow("Test data 2") << 5 << 10 << 10;
    QTest::newRow("Test data 3") << 10 << 5 << 10;
    QTest::newRow("Test data 4") << -5 << -10 << -5;
    QTest::newRow("Test data 5") << -10 << -5 << -5;
    QTest::newRow("Test data 6") << 5 << -10 << 5;
    QTest::newRow("Test data 7") << -5 << 10 << 10;

}

/*!
 * Табличный тест по сравнению результата работы метода myClass.max и ожидаемого значения.
 */
void MyFirstUnitTest::TablTestMax()
{
    MyClass mc;
    QFETCH(int, first);
    QFETCH(int, second);
    QFETCH(int, expected);
    int actual = mc.max(first, second);
    QCOMPARE(actual, expected);

}

QTEST_APPLESS_MAIN(MyFirstUnitTest)

#include "tst_myfirstunittest.moc"
